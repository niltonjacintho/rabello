import 'package:carousel_slider/carousel_slider.dart';
import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
// import 'package:imagebutton/imagebutt.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

final List<String> imgList = [
  'assets/images/rabello.jpg',
  'assets/images/rabello2.jpg',
  'assets/images/familia1.jpg',
  'assets/images/familia2.jpg',
  'assets/images/familia3.jpg'
];

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red[900],
      // appBar: AppBar(
      //   title: Text(widget.title),
      // ),
      body: ImageSliderDemo(),
      //  Align(
      //   alignment: Alignment.center,
      //   child: Padding(
      //     padding: EdgeInsets.all(20),
      //     child: Image.network(
      //       "https://i.ibb.co/qYNqWFj/Rabello.png",
      //       scale: 0.6,
      //     ),
      //   ),
      // ),
      bottomNavigationBar: FFNavigationBar(
        theme: FFNavigationBarTheme(
          barBackgroundColor: Colors.white,
          selectedItemBorderColor: Colors.transparent,
          selectedItemBackgroundColor: Colors.green,
          selectedItemIconColor: Colors.white,
          selectedItemLabelColor: Colors.black,
          showSelectedItemShadow: false,
          barHeight: 70,
        ),
        selectedIndex: selectedIndex,
        onSelectTab: (index) {
          print(index);
          setState(() {
            selectedIndex = index;
          });
        },
        items: [
          FFNavigationBarItem(
            iconData: Icons.contact_support_rounded,
            label: 'Votar em mim??',
          ),
          FFNavigationBarItem(
            iconData: Icons.people,
            label: 'Testemunhos',
            selectedBackgroundColor: Colors.orange,
          ),
          FFNavigationBarItem(
            iconData: Icons.construction_outlined,
            label: 'Realizações?',
            selectedBackgroundColor: Colors.purple,
          ),
        ],
      ),
      // );
    );
  }
}

class ImageSliderDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: Text('Image slider demo')),
      body: Container(
          child: CarouselSlider(
        options: CarouselOptions(
          autoPlay: true,
          aspectRatio: 0.6,
          viewportFraction: 1.0,
          enlargeCenterPage: true,
          enlargeStrategy: CenterPageEnlargeStrategy.height,
        ),
        items: imgList
            .map((item) => Container(
                  child: Center(child: Image.asset(item, scale: 0.7)),
                ))
            .toList(),
      )),
    );
  }
}
